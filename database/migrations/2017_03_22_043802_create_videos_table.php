<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->timestamps();
        });

        Schema::create('reference_video', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reference_id')->unsigned();
            $table->integer('video_id')->unsigned();
            $table->time('start_at');
            $table->time('end_at');
            $table->text('video_notes');
            $table->timestamps();
        });

        Schema::table('videos', function ($table){

            $table->foreign('user_id')->references('id')
                    ->on('users')
                    ->onDelete('cascade');
        });

        Schema::table('reference_video', function ($table){

            $table->foreign('reference_id')->references('id')
                    ->on('references')
                    ->onDelete('cascade');

            $table->foreign('video_id')->references('id')
                    ->on('videos')
                    ->onDelete('cascade');
            });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
        Schema::dropIfExists('reference_video');
        Schema::enableForeignKeyConstraints('videos');
    }
}
