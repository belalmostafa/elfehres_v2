<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->string('url');
            $table->string('author');
            $table->timestamps();
        });


        Schema::create('book_reference', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reference_id')->unsigned();
            $table->integer('book_id')->unsigned();
            $table->integer('from_page');
            $table->integer('to_page');
            $table->text('book_notes');
            $table->timestamps();
        });

        Schema::table('book_reference', function ($table) {

            $table->foreign('reference_id')->references('id')
                ->on('references')
                ->onDelete('cascade');

            $table->foreign('book_id')->references('id')
                ->on('books')
                ->onDelete('cascade');
        });

        Schema::table('books', function ($table) {

            $table->foreign('user_id')->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
        Schema::dropIfExists('book_reference');
        Schema::enableForeignKeyConstraints('books');

    }
}
