<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        Schema::create('reference_tag', function (Blueprint $table) {
            $table->integer('reference_id')->unsigned();
            $table->integer('tag_id')->unsigned();
            $table->primary(['reference_id','tag_id']);
            $table->timestamps();
        });

        Schema::table('tags', function ($table){
            $table->foreign('user_id')->references('id')
                    ->on('users')
                    ->onDelete('cascade');
        });


        Schema::table('reference_tag', function ($table){
            $table->foreign('reference_id')->references('id')
                    ->on('references')
                    ->onDelete('cascade');

            $table->foreign('tag_id')->references('id')
                    ->on('tags')
                    ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
        Schema::dropIfExists('tag_reference');
    }
}
