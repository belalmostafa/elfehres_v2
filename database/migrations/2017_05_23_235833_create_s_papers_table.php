<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSPapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_papers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('author')->nullable();
            $table->integer('user_id')->unsigned();

            $table->timestamps();
        });

        Schema::create('reference-s_paper', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reference_id')->unsigned();
            $table->integer('s_paper_id')->unsigned();
            $table->integer('from_page');
            $table->integer('to_page');
            $table->text('notes');
            $table->timestamps();
        });

        Schema::table('reference-s_paper', function ($table) {

            $table->foreign('reference_id')->references('id')
                ->on('references')
                ->onDelete('cascade');

            $table->foreign('s_paper_id')->references('id')
                ->on('s_papers')
                ->onDelete('cascade');
        });

        Schema::table('s_papers', function ($table) {

            $table->foreign('user_id')->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_papers');
    }
}
