<?php

use Illuminate\Database\Seeder;
use App\Reference;
use App\Video;

class ReferenceVideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $reference_ids = Reference::pluck('id')->all();
        $video_ids    = Video::pluck('id')->all();
        $faker = Faker\Factory::create();

        foreach (range(1, 30) as $index) {
            DB::table('reference_video')->insert([
                'reference_id' => $faker->randomElement($reference_ids),
                'video_id'    => $faker->randomElement($video_ids),
                'start_at'     => $faker->time(),
                'end_at'     => $faker->time(),
                'video_notes'     => $faker->paragraph(5),

            ]);

        }

    }
}
