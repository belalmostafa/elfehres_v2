<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    private $tables = [
        'users',
        'references',
        'tags',
        'reference_tag',
        'books',
        'book_reference',
        'videos',
        'reference_video',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->cleanDatabse();
        $this->call(UserSeeder::class);
        $this->call(ReferenceSeeder::class);
        $this->call(TagSeeder::class);
        $this->call(TagReferenceSeeder::class);
        $this->call(BookSeeder::class);
        $this->call(BookReferenceSeeder::class);
        $this->call(VideoSeeder::class);
        $this->call(ReferenceVideoSeeder::class);
    }

    public function cleanDatabse()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        foreach ($this->tables as $table){
            DB::table($table)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
