<?php

use Illuminate\Database\Seeder;
use App\Video;

class VideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();
        $user_ids = \App\User::pluck('id')->all();

        foreach (range(1,30) as $index){
            Video::create([
                'title'     => $faker->sentence(2),
                'user_id'   => $faker->randomElement($user_ids),
                'url'      => $faker->url,
            ]);
        }
    }
}
