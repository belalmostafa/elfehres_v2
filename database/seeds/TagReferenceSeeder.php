<?php

use Illuminate\Database\Seeder;
use App\Reference;
use App\Tag;


class TagReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //
        $reference_ids = Reference::pluck('id')->all();
        $tag_ids    = Tag::pluck('id')->all();
        $faker = Faker\Factory::create();

        foreach (range(1, 10) as $index) {
            DB::table('reference_tag')->insert([
                'reference_id' => $faker->randomElement($reference_ids),
                'tag_id'    => $faker->randomElement($tag_ids),

            ]);

        }
    }
}
