<?php

use Illuminate\Database\Seeder;
use App\Reference;
use App\Book;

class BookReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $reference_ids = Reference::pluck('id')->all();
        $book_ids    = Book::pluck('id')->all();
        $faker = Faker\Factory::create();

        foreach (range(1, 30) as $index) {
            DB::table('book_reference')->insert([
                'reference_id' => $faker->randomElement($reference_ids),
                'book_id'    => $faker->randomElement($book_ids),
                'from_page'     => $faker->randomDigit,
                'to_page'     => $faker->randomDigit,
                'book_notes'     => $faker->paragraph(5),

            ]);

        }
    }
}
