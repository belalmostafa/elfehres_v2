<?php

use Illuminate\Database\Seeder;
use App\Tag;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user_ids = \App\User::pluck('id')->all();
        $faker = Faker\Factory::create();


        foreach (range(1,30) as $index){
            Tag::create([
                'name'     => $faker->unique()->word,
                'user_id'   => $faker->randomElement($user_ids),
            ]);
        }
    }
}
