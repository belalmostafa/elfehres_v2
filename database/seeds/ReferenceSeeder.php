<?php

use Illuminate\Database\Seeder;
use App\Reference;
use App\User;

class ReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        $user_ids = User::pluck('id')->all();


        foreach (range(1,30) as $index){
            Reference::create([
                'title'     => $faker->sentence(2),
                'notes'      => $faker->paragraph(4),
                'user_id'       => $faker->randomElement($user_ids)
            ]);
        }
    }
}
