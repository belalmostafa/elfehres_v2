<?php

use Illuminate\Database\Seeder;
use App\Book;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_ids = \App\User::pluck('id')->all();
        $faker = Faker\Factory::create();


        foreach (range(1,30) as $index){
            Book::create([
                'title'     => $faker->sentence(2),
                'user_id'   => $faker->randomElement($user_ids),
                'url'      => $faker->url,
                'author'      => $faker->name,
            ]);
        }
    }
}
