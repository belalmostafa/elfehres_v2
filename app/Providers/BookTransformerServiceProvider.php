<?php

namespace App\Providers;

use App\Http\Controllers\elfehres\transformers\BookTransformer;
use Illuminate\Support\ServiceProvider;

class BookTransformerServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $bookTransformerInstance = new BookTransformer();
        $this->app->instance(BookTransformer::class, $bookTransformerInstance);
    }

    public function provides()
    {
        return [BookTransformer::class];
    }
}
