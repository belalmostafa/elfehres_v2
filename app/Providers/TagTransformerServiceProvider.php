<?php

namespace App\Providers;

use App\Http\Controllers\elfehres\transformers\TagTransformer;
use Illuminate\Support\ServiceProvider;

class TagTransformerServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $tagTransformerInstance = new TagTransformer();
        $this->app->instance(TagTransformer::class, $tagTransformerInstance);
    }

    public function provides()
    {
        return [TagTransformer::class];
    }
}
