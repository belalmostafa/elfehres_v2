<?php

namespace App\Providers;

use App\Http\Controllers\elfehres\transformers\UserTransformer;
use Illuminate\Support\ServiceProvider;

class UserTransformerServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $userTransformer = new UserTransformer();
        $this->app->instance(UserTransformer::class, $userTransformer);
    }

    public function provides()
    {
        return [UserTransformer::class];
    }
}
