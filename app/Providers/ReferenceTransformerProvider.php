<?php

namespace App\Providers;

use App\Http\Controllers\elfehres\transformers\ReferenceTransformer;
use Illuminate\Support\ServiceProvider;

class ReferenceTransformerProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $referenceTransformerInstance = new ReferenceTransformer();
        $this->app->instance(ReferenceTransformer::class, $referenceTransformerInstance);
    }

    public function provides()
    {
        return [ReferenceTransformer::class];
    }
}
