<?php

namespace App\Providers;

use App\Http\Controllers\elfehres\transformers\VideoTransformer;
use Illuminate\Support\ServiceProvider;

class VideoTransformerServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $videoTransformerInstance = new VideoTransformer();
        $this->app->instance(VideoTransformer::class, $videoTransformerInstance);
    }

    public function provides()
    {
        return [VideoTransformer::class];
    }
}
