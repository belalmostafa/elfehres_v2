<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //
    public function references(){
        return $this->belongsToMany(Reference::class)
                    ->withPivot('start_at','end_at','video_notes')
                    ->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
