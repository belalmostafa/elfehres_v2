<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function references(){
        return $this->belongsToMany(Reference::class)->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
