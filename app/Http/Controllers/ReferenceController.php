<?php

namespace App\Http\Controllers;

use App\Http\Controllers\elfehres\transformers\BookTransformer;
use App\Http\Controllers\elfehres\transformers\ReferenceTransformer;
use App\Http\Controllers\elfehres\transformers\TagTransformer;
use App\Http\Controllers\elfehres\transformers\VideoTransformer;
use App\Reference;
use Illuminate\Http\Request;


//TODO handle duplicate entries


class ReferenceController extends ApiController
{
    /**
     * @var ReferenceTransformer
     */
    protected $referenceTransformer;
    /**
     * ReferenceController constructor.
     */
    public function __construct()
    {
        $this->referenceTransformer = resolve(ReferenceTransformer::class);
        $this->middleware('jwt.auth', ['except' => ['index','show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $limit = (request('limit')) ? (request('limit') < 20) ? request('limit') : 20 :3;
        $references = Reference::paginate($limit);
        return response($references->load('user','tags', 'books.user', 'videos.user')); //TODO :: handle transforming the response before sending it
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     * @return \Illuminate\Http\JsonResponse
     * @internal param Request $request
     */
    public function store()
    {

            $this->validate(request(),[
                'referenceTitle'        => 'required',
                'referenceDescription'  =>'required|max:1000',
                'tags.*'                => 'required|json|unique'
            ]);

            $isSaved = $this->persistReference();
            return  ($isSaved) ? $this->respond('Reference is added successfully!') : $this->respond(
                                                                                        ['data' => 'failed to save reference, please try again',
                                                                                         'code' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $reference = Reference::find($id);

        if(! $reference){
            return $this->respondNotFound("Reference is not found");
        }

        $fullRawReferenceObject = $reference->load('user','tags.user','books.user','videos.user')->toArray();
        $transformedRelatedReferenceObjectData = $this->transformRawReferenceObject([
                                                'tags'  => $fullRawReferenceObject['tags'],
                                                'books'  => $fullRawReferenceObject['books'],
                                                'videos'  => $fullRawReferenceObject['videos'],
                                                ]);

        unset(  $fullRawReferenceObject['tags'],
                $fullRawReferenceObject['books'],
                $fullRawReferenceObject['videos']
            );
        $transformedReference= $this->referenceTransformer->transform($fullRawReferenceObject, $transformedRelatedReferenceObjectData);

        return $this->respond($transformedReference); //TODO unhandled response
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reference = Reference::find($id);
        return ($reference->forceDelete()) ? $this->respond("Reference Has been Deleted") : $this->respondWithError("Something Happened! Reference couldn't be deleted") ;

    }

    /**
     * @param Reference $reference
     * @param $referenceTitle
     */
    public function setReferenceTitle(Reference $reference  , $referenceTitle)
    {
        $reference->title = $referenceTitle;
    }

    /**
     * @param Reference $reference
     * @param $referenceNote
     */
    public function setReferenceNotes(Reference $reference  , $referenceNote)
    {
        $reference->notes = $referenceNote;
    }

    /**
     * @param $relatedReferenceObjectsTobeTransformedForSingleReferenceDisplay
     * @return array
     */
    public function transformRawReferenceObject($relatedReferenceObjectsTobeTransformedForSingleReferenceDisplay)
    {
        return array_merge(['tags'      => $this->transformTagsForReference($relatedReferenceObjectsTobeTransformedForSingleReferenceDisplay['tags'])],
                            ['books'    => $this->transformBooksForReference($relatedReferenceObjectsTobeTransformedForSingleReferenceDisplay['books'])],
                            ['videos'   => $this->transformVideosForReference($relatedReferenceObjectsTobeTransformedForSingleReferenceDisplay['videos'])]
            );
    }

    /**
     * @param array $tags
     * @return mixed
     */
    public function transformTagsForReference(array $tags)
    {
        $tagTransformer = resolve(TagTransformer::class);
        $transformedTags = $tagTransformer->transformTagsForPatchReferenceDisplay($tags);
        unset($tagTransformer);
        return $transformedTags;
    }

    /**
     * @param array $books
     * @return mixed
     */
    public function transformBooksForReference(array $books)
    {
        $booksTransformer = resolve(BookTransformer::class);
        $transformedBooks = $booksTransformer->transformBooksForPatchReferenceDisplay($books);

        unset($booksTransformer);
        return $transformedBooks;
    }

    /**
     * @param array $videos
     * @return mixed
     */
    public function transformVideosForReference(array $videos)
    {
        $videoTransformer = resolve(VideoTransformer::class);
        $transferedVideos = $videoTransformer->transformVideosForPatchReferenceDisplay($videos);

        unset($videoTransformer);
        return $transferedVideos;
    }


    /**
     * @return bool
     */
    public function persistReference()
    {
        $reference = new Reference();
        $user = $this->getAuthUser();
        $reference->user()->associate($user);
        $this->setReferenceTitle($reference ,request('referenceTitle'));
        $this->setReferenceNotes($reference ,request('referenceDescription'));
        $referenceIsSaved = $reference->save();

        $tags =$this->parseJsonObject(request('tags'));
        $tagController = new TagController();

        $notSavedTags = [];

        foreach($tags as $tag){

            //TODO handle approperiate response if tag is not saved, tag is returned on success
           $isTagSaved = $tagController->persistTag($reference, $tag['tagName']);

            //TODO hanlde returning list of tags that aren't returned properly
            array_push($notSavedTags, $tag);
        }

        return $referenceIsSaved;

    }
}
