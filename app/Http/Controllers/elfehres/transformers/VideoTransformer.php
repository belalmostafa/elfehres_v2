<?php

namespace App\Http\Controllers\elfehres\transformers;
use Carbon\Carbon;

class VideoTransformer extends Transformer
{


    /**
     * @param $video
     * @return array
     */
    public function transform($video, array $relatedObjects = null){
         $videoObject = [
            'id'    =>$video['id'],
            'title' => $video['title'],
            'url'  => $video['url'],
            'createdAt' => Carbon::parse($video['created_at'])->diffForHumans(),
        ];

        return ($relatedObjects) ? array_merge($videoObject , $relatedObjects) : $videoObject;
    }


    /**
     * @param array $videos
     * @return array
     */
    public function transformVideosForPatchReferenceDisplay(array $videos)
    {
        return array_map([$this, 'transformVideoForSingleReference'], $videos);
    }


    /**
     * @param $video
     * @param array|null $relatedObjects
     * @return array
     */
    public function transformVideoForSingleReference($video, array $relatedObjects = null)
    {

        $userTransformer    = resolve(UserTransformer::class);
        $transformedUserForVideo    = $userTransformer->transformUserForSingleDisplay($video['user']);

        $videoObject = [
            'id'    =>$video['id'],
            'title' => $video['title'],
            'url'  => $video['url'],
            'createdAt' => Carbon::parse($video['created_at'])->diffForHumans(),
            'startAt'  =>  $video['pivot']['start_at'],
            'endAt'    =>  $video['pivot']['end_at'],
            'videoNotes'    =>  $video['pivot']['video_notes'],
        ];

        $videoUserObject = ['user'  => $transformedUserForVideo];

        return ($relatedObjects) ? array_merge($videoObject, $relatedObjects) : array_merge($videoObject, $videoUserObject);

    }
}