<?php

namespace App\Http\Controllers\elfehres\transformers;
use Carbon\Carbon;

class BookTransformer extends Transformer
{


    /**
     * @param $reference
     * @return array
     */
    public function transform($book, array $relatedObjects = null){ //TODO use the right columns for a book transformer

        $bookObject = [
            'id'            => $book['id'],
            'title'         => $book['title'],
            'url'           => $book['url'],
            'author'        => $book['author'],
            'createdAt'     => Carbon::parse($book['created_at'])->diffForHumans(),
            'fromPage'      => $book['pivot']['from_page'],
            'toPage'        => $book['pivot']['to_page'],
            'bookNotes'     => $book['pivot']['book_notes'],
        ];

        return array_merge($bookObject, $relatedObjects);
    }


    /**
     * @param array $books
     * @return array
     */
    public function transformBooksForPatchReferenceDisplay(array $books)
    {

        return array_map([$this, 'transformBookForSingleReference'], $books);
    }

    /**
     * @param $book
     * @param array|null $relatedObjects
     * @return array
     */
    public function transformBookForSingleReference($book, array $relatedObjects = null)
    {
        $userTransformer    = resolve(UserTransformer::class);
        $transformedUserForBook    = $userTransformer->transformUserForSingleDisplay($book['user']);

        $bookObject = [
            'id'            =>  $book['id'],
            'title'         =>  $book['title'],
            'url'           =>  $book['url'],
            'author'        =>  $book['author'],
            'createdAt'     =>  Carbon::parse($book['created_at'])->diffForHumans(),
            'fromPage'      =>  $book['pivot']['from_page'],
            'toPage'        =>  $book['pivot']['to_page'],
            'bookNotes'     =>  $book['pivot']['book_notes'],
        ];

        $bookUserObject  = ['user'  => $transformedUserForBook];

        return ($relatedObjects) ? array_merge($bookObject, $relatedObjects) : array_merge($bookObject, $bookUserObject);

    }
}