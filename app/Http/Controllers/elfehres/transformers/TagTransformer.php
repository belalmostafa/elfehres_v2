<?php

namespace App\Http\Controllers\elfehres\transformers;

class TagTransformer extends Transformer
{
    /**
     * @param $tag
     * @param array|null $relatedObjects
     * @return array
     */
    public function transform($tag, array $relatedObjects = null){

        $tagObject = [
            'id'    => $tag['id'],
            'name'  =>  $tag['name']
        ];

        return ($relatedObjects) ? array_merge($tagObject, $relatedObjects) : $tagObject;
    }

    /**
     * @param array $tags
     * @return array
     */
    public function transformTagsForPatchReferenceDisplay(array $tags)
    {

        return array_map([$this, 'transformTagForSingleReference'], $tags);
    }

    /**
     * @param $tag
     * @param array|null $relatedObjects
     * @return array
     */
    public function transformTagForSingleReference($tag, array $relatedObjects = null)
    {
        $userTransformer    = resolve(UserTransformer::class);
        $transformedUserForTag     = $userTransformer->transformUserForSingleDisplay($tag['user']);

        $tagObject = [
            'id'    => $tag['id'],
            'name'  =>  $tag['name']
        ];

        $TagUserObject = ['user'    =>  $transformedUserForTag];

        return ($relatedObjects) ? array_merge($tagObject , $relatedObjects) : array_merge($tagObject, $TagUserObject);

    }
}