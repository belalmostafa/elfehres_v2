<?php

namespace App\Http\Controllers\elfehres\transformers;

use phpDocumentor\Reflection\Types\Boolean;
use Carbon\Carbon;

class ReferenceTransformer extends Transformer
{


    /**
     * @param $reference
     * @param array $relatedObjects
     * @return array
     * @internal param array|null $extra
     * @internal param bool $full
     */
    public function transform($reference, array $relatedObjects = null){

        $userTransformer    = resolve(UserTransformer::class);
        $transformedUserForReference    = $userTransformer->transformUserForSingleDisplay($reference['user']);

        $referenceObject =  [
            'id'                =>  $reference['id'],
            'title'             =>  $reference['title'],
            'createdAt'         =>  Carbon::parse($reference['created_at'])->diffForHumans(),
            'updatedAt'         =>  Carbon::parse($reference['updated_at'])->diffForHumans(),
            'referenceNotes'    =>  $reference['notes'],
        ];

        $referenceUserObject     =  ['user' => $transformedUserForReference];

        return array_merge($referenceObject, $referenceUserObject, $relatedObjects);
    }
}