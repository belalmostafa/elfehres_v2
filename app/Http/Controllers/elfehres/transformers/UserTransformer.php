<?php


namespace App\Http\Controllers\elfehres\transformers;


/**
 * Class UserTransformerextends
 * @package App\Http\Controllers\elfehres\transformers
 */
class UserTransformer extends Transformer
{


    /**
     * @param $user
     * @param array $relatedObjects
     * @return array
     * @internal param $reference
     */
    public function transform($user, array $relatedObjects = null){ //TODO use the right columns for a user transformer

        $userObject = [
            'id'            => $user['id'],
            'name'         => $user['name'],
        ];

        return array_merge($userObject, $relatedObjects);
    }


    /**
     * @param array $users
     * @return array
     */
    public function transformUsersForPatchDisplay(array $users)
    {

        return array_map([$this, 'transformUserForSingleDisplay'], $users);
    }

    /**
     * @param $user
     * @param array|null $relatedObjects
     * @return array
     */
    public function transformUserForSingleDisplay($user, array $relatedObjects = null)
    {
        $userObject = [
            'id'            => $user['id'],
            'name'         => $user['name'],
        ];
        return ($relatedObjects) ? array_merge($userObject, $relatedObjects) : $userObject;

    }
}