<?php


namespace App\Http\Controllers\elfehres\transformers;

abstract class Transformer
{
    /**
     * @param array $items
     * @param array $relatedObjects
     * @return array
     */
    public function transformCollection(array $items, array $relatedObjects = []){

        return array_map([$this, 'transform'],$items, $relatedObjects);
    }

    /**
     * @param $item
     * @param array $relatedObjects
     * @return mixed
     */
    public abstract function transform($item, array $relatedObjects = []);
}


