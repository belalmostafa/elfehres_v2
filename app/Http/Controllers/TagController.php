<?php

namespace App\Http\Controllers;

use App\Reference;
use App\Tag;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\String_;

/**
 * Class TagController
 * @package App\Http\Controllers
 */
class TagController extends Controller
{
    //TODO relate tag to user
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Reference $reference
     * @param String $tagName
     * @return bool : true upon successfully adding a tag, false otherwise
     */
    public function persistTag(Reference $reference = null, $tagName){
        $tag = Tag::where('name', $tagName)->get()->first();
        if($tag){
            $tag->references()->attach($reference);
            return $tag;

        }else{
            $tag = new Tag();
            $this->setTagName($tag, $tagName);
            $isTagSaved = $tag->save();
            ($isTagSaved) ?: $tag->references()->attach($reference);
            return ($isTagSaved) ? $tag : false;
        }

    }

    /**
     * @param Tag $tag
     * @param $tagName
     */
    public function setTagName(Tag $tag, $tagName){
        $tag->name = $tagName;
    }
}
