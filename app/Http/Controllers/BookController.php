<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Controllers\elfehres\transformers\BookTransformer;
use App\Http\Controllers\elfehres\transformers\ReferenceTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;

//TODO handle duplicate entries

/**
 * Class BookController
 * @package App\Http\Controllers
 */
class BookController extends ApiController
{
    /**
     * @var BookTransformer
     */
    protected $bookTransformer;

    /**
     * BookController constructor.
     * @param BookTransformer $bookTransformer
     */
    public function __construct(BookTransformer $bookTransformer)
    {
        $this->bookTransformer = $bookTransformer;
        $this->middleware('jwt.auth', ['except' => ['index','show'] ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $limit = (request('limit')) ? (request('limit') < 20) ? request('limit') : 20 : 3;
        $books = Book::paginate($limit);
        return response($books->load('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make(request()->all(), [
            'bookTitle'         => 'required',
            'bookAuthor'        => 'required',
            'bookUrl'           => 'required',
            'bookNotes'         => 'required',
            'reference_id'      => 'required|integer|min:1',
            'from_page'         => 'required|integer|min:0',
            'to_page'           => 'required|integer|min:0',

        ]);

        if ($validator->fails()){
            return $this->setStatusCode(422)->respondWithError("Something wrong with the fields");
        }

        $isBookSaved = $this->persistBook();

        return ($isBookSaved) ? $this->respond("Book is saved") : $this->respondWithError("Couldn't persist book");
    }

    /**
     * Display the specified resource.
     *
     * @param Book $book
     * @return Book
     * @internal param int $id
     */
    public function show(Book $book)
    {

        $book->load('user')->find($book);
        return $book;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        return ($book->forceDelete()) ? $this->respond("Book Has been Deleted") : $this->respondWithError("Something Happened! Book couldn't be deleted") ;

    }

    /**
     * @return bool
     */
    public function persistBook()
    {

        $book = new Book();
        $user = $this->getAuthUser();
        $book->user()->associate($user);

        $book = $this->setBookTitle($book, request('bookTitle'));
        $book = $this->setBookAuthor($book, request('bookAuthor'));
        $book = $this->setBookUrl($book, request('bookUrl'));

        if($book->save()){
            $bookReferencePivotTableParams          = $this->getBookReferencePivotTableParams();
            $isReferenceSuccessfullyAttachedToBook  = $this->attachReferenceToBook($book, $bookReferencePivotTableParams);

            unset($book);
            return($isReferenceSuccessfullyAttachedToBook) ? true : false;
        }

        unset($book);
        return false;
    }

    /**
     * @param Book $book
     * @param $bookReferencePivotTableParams
     * @return bool
     */
    public function attachReferenceToBook(Book $book, $bookReferencePivotTableParams)
    {
           $book->references()->attach(request('reference_id'), array(
                'from_page' => $bookReferencePivotTableParams['from_page'],
                'to_page' => $bookReferencePivotTableParams['to_page'],
                'book_notes' => $bookReferencePivotTableParams['book_notes']));
            //TODO handle if reference is not attached to remove the previous book
            return ($book->references()->find(request('reference_id'))) ? true : false;


    }

    /**
     * @return array
     */
    public function getBookReferencePivotTableParams()
    {
        return ['reference_id'  =>  request('reference_id'),
                'from_page'     =>  request('from_page'),
                'to_page'       =>  request('to_page'),
                'book_notes'    =>  request('bookNotes')];
    }

    /**
     * @param Book $book
     * @param $bookTitle
     * @return Book
     */
    public function setBookTitle(Book $book, $bookTitle)
    {
        $book->title = $bookTitle;
        return $book;
    }

    /**
     * @param Book $book
     * @param $bookAuthor
     * @return Book
     */
    public function setBookAuthor(Book $book, $bookAuthor)
    {
        $book->author = $bookAuthor;
        return $book;
    }

    /**
     * @param Book $book
     * @param $bookUrl
     * @return Book
     */
    public function setBookUrl(Book $book, $bookUrl)
    {
        $book->url  = $bookUrl;
        return $book;
    }

}
