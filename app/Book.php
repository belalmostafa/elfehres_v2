<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    public function references(){
        return $this->belongsToMany(Reference::class)
            ->withPivot('from_page','to_page','book_notes')
            ->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getRouteKeyName()
    {
        return 'title';
    }
}
