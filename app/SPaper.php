<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SPaper extends Model
{
    public function references(){
        return $this->belongsToMany(Reference::class)
            ->withPivot('from_page','to_page','notes')
            ->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getRouteKeyName()
    {
        return 'title';
    }
}
