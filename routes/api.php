<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



Route::post('authenticate', 'AuthenticateController@authenticate');
Route::get('refresh', 'AuthenticateController@refresh');

Route::resource('reference', 'ReferenceController');
Route::resource('tag', 'TagController');
Route::resource('book', 'BookController');
Route::resource('video', 'VideoController');
Route::resource('sci_paper', 'SPaperController');